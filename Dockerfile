FROM python:slim-bullseye
RUN python3 -m pip install mysql-connector-python flask
COPY joke_reader.py /usr/local/bin/
RUN chmod +x /usr/local/bin/joke_reader.py
WORKDIR /
#EXPOSE 8000
#ENTRYPOINT [ "python3" ]
CMD ["joke_reader.py"]
