#!/usr/bin/env python3

from flask import Flask, jsonify
import mysql.connector

app = Flask(__name__)

# Connect to the MySQL server
mydb = mysql.connector.connect(
    host="storage-mysql.storage-mysql.svc.cluster.local",
    user="chuck_norris_joke",
    password="Dtamtus_007",
    database="chuck_norris_joke",
    auth_plugin='mysql_native_password'
)
mycursor = mydb.cursor()

# Endpoint to get all jokes
@app.route('/jokes', methods=['GET'])
def get_all_jokes():
    mycursor.execute("SELECT joke FROM jokes")
    jokes = [joke[0] for joke in mycursor.fetchall()]
    return jsonify({'jokes': jokes})

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000)
